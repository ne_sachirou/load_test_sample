require "sidekiq"

Sidekiq.configure_server do |_config|
  schedules = YAML.load(ERB.new(File.read(File.join(Rails.root, "config/schedule.yml"))).result)[Rails.env]
  Sidekiq::Cron::Job.load_from_hash schedules
end

Sidekiq.configure_client do |config|
end
